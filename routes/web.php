<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\VideoController;
use App\Http\Controllers\ImageUploadController;
use App\Http\Controllers\ProgressBarController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');

Route::get('/video',[VideoController::class, 'makeVideo'])->name('makevideo');
Route::post('image-upload', [ImageUploadController::class, 'imageUploadPost'])->name('image.upload.post');
Route::get('progress', [ProgressBarController::class, 'index'])->name('progress');
Route::get('get-video',[VideoController::class, 'getVideo'])->name('getVideo');
