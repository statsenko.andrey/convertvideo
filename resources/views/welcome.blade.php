<!DOCTYPE html>
<html>
<head>
    <title>SlideShow</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/iziToast.min.css') }}">
</head>
<body>
<style>
    .container {
        width: 1200px;
        margin: 50px auto;
        font-family: sans-serif;
    }


</style>
<div class="container" style="pointer-events: all;">
    <div class="panel panel-primary">
        <div class="panel-heading">


            <h2>Загрузите файлы изображений для создания видео (слайд шоу)</h2>
            <br>
            <br>
            <br>
        </div>
        <div class="panel-body">


            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Упс!</strong> Какие-то проблемы с изображением.
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form id="form" action="{{ route('image.upload.post') }}" method="POST" enctype="multipart/form-data">


                @csrf
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <input id="image1" type="file" name="image[1]" class="form-control" required>
                        </div>
                        <div class="col-md-2">
                            <input id="number1" type="number" name="number[1]" class="form-control"
                                   placeholder="время(сек)" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <input id="image2" type="file" name="image[2]" class="form-control" required>
                        </div>
                        <div class="col-md-2">
                            <input id="number2" type="number" name="number[2]" class="form-control"
                                   placeholder="время(сек)" required>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <input id="image3" type="file" name="image[3]" class="form-control" required>
                        </div>
                        <div class="col-md-2">
                            <input id="number3" type="number" name="number[3]" class="form-control"
                                   placeholder="время(сек)" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <input id="image4" type="file" name="image[4]" class="form-control" required>
                        </div>
                        <div class="col-md-2">
                            <input id="number4" type="number" name="number[4]" class="form-control"
                                   placeholder="время(сек)" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <input id="image5" type="file" name="image[5]" class="form-control" required>
                        </div>
                        <div class="col-md-2">
                            <input id="number5" type="number" name="number[5]" class="form-control"
                                   placeholder="время(сек)" required>
                        </div>
                    </div>

                </div>
                <input type="submit" class="btn btn-success" value="Загрузить и преобразовать все изображения">
            </form>

        </div>
    </div>
</div>
@if ($message = Session::get('success'))
    <div class="alert alert-success alert-block">
        <center>
            <strong>{{ $message }}</strong>
        </center>
    </div>
    <div class="progress col-md-6 mx-auto">
        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="0"
             aria-valuemin="0" aria-valuemax="100" style="width: 0%">0%
        </div>
    </div>
@endif

<div id="conv-table" class="table" style="display: none">
    <table class="table">
        <th>id</th>
        <th>Видео</th>
        <th>Дата начала создания</th>
        <th>Дата окончания создания</th>
        <th>Статус</th>
        <tr>
            <td id="conv-id"></td>
            <td id="conv-video">
            <td id="conv-start">
            <td id="conv-end"></td>
            <td id="conv-status"></td>
        </tr>
    </table>
</div>
<div id="conv-error" class="alert alert-danger col-md-12 mx-auto" style="display: none">
    <strong>Упс!</strong> Какие-то проблемы с созданием видео.

</div>

</body>
</html>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
        integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
        integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF"
        crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="{{ asset('js/iziToast.min.js') }}"></script>
@if ($message = Session::get('success'))

    <script>

        $('.container').css('pointer-events', 'none');
        var Url = "{{ url('/progress') }}";
        var video_id = "{{Session::get('video_id')}}"

        var checkprogress = setInterval(function () {
            jQuery.get(
                Url,
                {
                    data: video_id,
                }
            ).done(function (data) {
                    console.log(data);
                    if (data == 200) {
                        stopcheck();
                        $('.container').css('pointer-events', 'all');
                        getVideo();
                        iziToast.success({
                            timeout: 5000,
                            position: 'topCenter',
                            icon: 'fa fa-check',
                            title: 'Отлично.',
                            message: 'Видео создано, можете посмотреть его по ссылке внизу'
                        });
                        data = 100;
                    }
                    if (data == 404) {
                        stopcheck();
                        $('.container').css('pointer-events', 'all');
                        $('#conv-error').css('display', 'block');
                        iziToast.error({
                            timeout: 5000,
                            position: 'topCenter',
                            icon: 'fa fa-check',
                            title: 'Ошибка.',
                            message: 'Что-то пошло не так'
                        });
                        data = 0;
                    }
                    var percent = data + '%';
                    $('.progress-bar').html(percent);
                    $('.progress-bar').width(percent);

                }
            )
                .fail(function (data) {
                    }
                );
        }, 2000);

        function stopcheck() {
            clearInterval(checkprogress);
        }

        function getVideo() {
            var Url = "{{ url('/get-video') }}";
            var video_id = "{{Session::get('video_id')}}";
            jQuery.get(
                Url,
                {
                    data: video_id,
                }
            ).done(function (data) {

                video = JSON.parse(data);
                $('#conv-table').css('display', 'block');
                $('#conv-id').html(video.id);
                $('#conv-video').html('<a href="' + video.path + '" target="_blank">Посмотреть видео</a>');
                $('#conv-start').html(video.created_at);
                $('#conv-end').html(video.end);
                $('#conv-status').html(video.success);


            })

        }
    </script>
@endif


<script>


    {{--$(document).on("submit", "form", function(event)--}}
    {{--{--}}
    {{--    event.preventDefault();--}}
    {{--    var Url = "{{ url('/image-upload') }}";--}}
    {{--    jQuery.post({--}}
    {{--         Url,--}}
    {{--        data: new FormData(this),--}}

    {{--        processData: false,--}}
    {{--        contentType: false,--}}
    {{--        success: function (data, status)--}}
    {{--        {--}}

    {{--        },--}}
    {{--        error: function (xhr, desc, err)--}}
    {{--        {--}}


    {{--        }--}}
    {{--    });--}}

    {{--});--}}
    {{--    $( "form" ).on( "submit", function( event ) {--}}
    {{--        event.preventDefault();--}}
    {{--        var data = $( this ).serialize();--}}
    {{--        // console.log(data);--}}
    {{--console.log($("#image1").val());--}}
    {{--            var postUrl = "{{ url('/image-upload') }}";--}}
    {{--            // button.find('.fa-spinner').show();--}}
    {{--            jQuery.post(--}}
    {{--                postUrl,--}}
    {{--                {--}}

    {{--                    data: new FormData(this),--}}


    {{--                }--}}
    {{--            )--}}
    {{--                .done(function (data) {--}}
    {{--                  //  button.find('.fa-spinner').hide();--}}
    {{--console.log(data);--}}
    {{--                    // iziToast.success({--}}
    {{--                    //     timeout: 5000,--}}
    {{--                    //     position: 'topCenter',--}}
    {{--                    //     icon: 'fa fa-check',--}}
    {{--                    //     title: 'Отлично.',--}}
    {{--                    //     message: 'Изменения успешно сохранены'--}}
    {{--                    // });--}}
    {{--                    // setTimeout(function(){--}}
    {{--                    //     window.location.reload();--}}
    {{--                    // }, 3000);--}}

    {{--                })--}}
    {{--                // .fail(function () {--}}
    {{--                //     button.find('.fa-spinner').hide();--}}
    {{--                //     iziToast.error({--}}
    {{--                //         timeout: 5000,--}}
    {{--                //         position: 'topCenter',--}}
    {{--                //         icon: 'fa fa-exclamation-triangle',--}}
    {{--                //         title: 'Ошибка.',--}}
    {{--                //         message: "Сохранить не удалось"--}}
    {{--                //     });--}}
    {{--                //     //  window.location.reload();--}}
    {{--                // });--}}


    {{--    });--}}

</script>
