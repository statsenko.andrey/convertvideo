<?php

namespace App\Jobs;

use App\Models\Video;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Illuminate\Support\Str;
use Throwable;

class ConvertVideoForDownloading implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $video;

    public function __construct(Video $video)
    {
        $this->video = $video;
    }


    public function handle()
    {
        ini_set('max_execution_time', 300);
        set_time_limit(300);
        $videoName = Str::uuid();
        $ffmpeg = 'C:\windows\system32\ffmpeg';
        $listPath = storage_path('app\list.txt');
        $outVideo = storage_path('app\public/' . $videoName . '.mp4');
        $log = storage_path('app\public\log.txt');
        $command = $ffmpeg . ' -f concat -safe 0 -i ' . $listPath . ' ' . $outVideo . ' > ' . $log . '  2<&1';
        $process = Process::fromShellCommandline($command);
        $process->setTimeout(360);
        $process->setIdleTimeout(300);
        $process->run();
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);

        }
        $this->video->update([
            'end' => Carbon::now(),
            'success' => 1,
            'path' => env('APP_URL').'/storage/'.$videoName . '.mp4',
        ]);
    }

    public function failed(Throwable $exception)
    {
        $this->video->update([
            'success' => 0,
        ]);
    }


}
