<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreVideoRequest;
use App\Models\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\Http\Controllers\VideoController;

class ImageUploadController extends Controller
{
    public function imageUploadPost(StoreVideoRequest $request)
    {

        $disk = Storage::disk('local');
        if ($disk->exists('list.txt')) $disk->delete('list.txt');
        if ($disk->exists('log.txt')) $disk->delete('log.txt');

        $length = count($request->image);
        for ($i = 1; $i < $length + 1; $i++) {
            $file = $request->image[$i];
            $imageName = $i . '.jpg';
            $image = Image::make($file);
            $image->resize(300, 300)->encode('jpg', 90)->save(public_path('images/') . $imageName, 90);
            $disk->append('list.txt',
                "file '" . public_path('images/') . $imageName . "'\r\nduration " . $request->number[$i]);
        }
        $video = Video::create();
        $vieocontroller = new VideoController();
        $vieocontroller->makeVideo($video);

        return redirect()->route('home')
            ->with('success', 'Изображения обработаны, приведены к единому размеру и формату, начался процесс создания видео')
            ->with('video_id', $video->id);

    }
}
