<?php

namespace App\Http\Controllers;

use App\Models\Video;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;
use Illuminate\Support\Str;
use App\Jobs\ConvertVideoForDownloading;

class VideoController extends Controller
{

    public function makeVideo($video)
    {
        $this->dispatch(new ConvertVideoForDownloading($video));
        return response()->json([
            'id' => $video->id,
        ], 201);
    }

    public function getVideo()
    {
        $video_id = ($_GET['data']);
        $video = Video::find($video_id);
        return json_encode($video);
    }


}
